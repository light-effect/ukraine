import {Axios} from "axios";
import UserInterface from "@/models/UserInterface";
import AuthorizationClient from "@/client/authorizationClient";
import store from "@/store";

export default class UserRepository {

    private readonly client: Axios;

    private readonly authClient: Axios;

    constructor(client: Axios) {
        this.client = client;
        this.authClient = AuthorizationClient;
    }

    public async postUser(user: UserInterface): Promise<string> {
        await this.client.post('/user', user).then().catch(error => {
            return  error.response.data.message;
        });

        return 'Success';
    }

    public async login(user: UserInterface): Promise<string | null> {
        let token: string | null = null;

        await this.authClient.post('/token', user).then(response => {
            token = response.data.data.token;
        }).catch(error => {
            throw error.response.data.message;
        });

        return token;
    }

    public async logout(): Promise<string> {
        await this.client.get('/logout').then(() => {
            store.dispatch('setUser', null);

            window.localStorage.removeItem('token');
        }).catch(error => {
            throw error.response.data.message;
        });

        return 'Success';
    }

    public async getUser(): Promise<UserInterface | null> {
        if (window.localStorage.getItem('token') === null) {
            return null;
        }

        let user = null;

        await this.client.get(
            '/account',
            {
                headers: {
                    Authorization: "Bearer " + window.localStorage.getItem('token')
                }
            }
        ).then(response => user = {
                id: response.data.id,
                email: response.data.email,
                firstName: response.data.firstName,
                lastName: response.data.lastName,
                username: response.data.username,
                location: store.getters.locationBySlug(response.data.location),
                type: response.data.type,
            }
        ).catch(() =>
            user = null
        );

        return user;
    }

    public async getVolunteers(): Promise<Array<UserInterface>> {
        let users: Array<UserInterface> = [];

        await this.client.get('/volunteers', {
            headers: {
                Authorization: 'Bearer ' + window.localStorage.getItem('token')
            }
        }).then(response => response.data.forEach((user: any) =>
            users.push({
                id: user.id,
                email: user.email,
                firstName: user.firstName,
                lastName: user.lastName,
                username: user.username,
                phone: user.phone,
                information: user.information,
                location: store.getters.locationBySlug(user.location),
                type: user.type,
            })
        ));

        return users;
    }
}