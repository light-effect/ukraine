import {Axios} from "axios";
import PageInterface from "@/models/PageInterface";
import ParamsInterface from "@/models/ParamsInterface";

export default class PageRepository {

    private readonly client: Axios;

    constructor(client: Axios) {
        this.client = client;
    }

    public async fetchPage(slug: string): Promise<PageInterface> {
        let page: PageInterface = {
            content: "",
            slug: "",
            title: "",
            id: 0
        };

        let params: ParamsInterface = { slug };

        await this.client.get('/pages', {params}).then(response => page = {
            id: response.data[0].ID,
            content: response.data[0].content.rendered,
            title: response.data[0].title.rendered,
            slug: response.data[0].slug,
        });

        return page;
    }
}