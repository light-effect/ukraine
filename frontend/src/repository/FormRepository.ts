import {Axios} from "axios";
import RequestInterface from "@/models/RequestInterface";
import formClient from "@/client/formClient";

export default class FormRepository {

    private readonly client: Axios;

    private readonly id: number = 20;

    constructor() {
        this.client = formClient;
    }

    public async postRequestForm(request: RequestInterface): Promise<boolean> {
        let result = false;
        let bodyFormData = new FormData();

        bodyFormData.append('fullName', request.fullName);
        bodyFormData.append('phone', request.phone);
        bodyFormData.append('location', request.location.name);
        bodyFormData.append('comment', request.comment);

        await this.client.post('/contact-form-7/v1/contact-forms/'+ this.id + '/feedback', bodyFormData, {
            headers:{
                'Content-Type':'multipart/form-data'
            }
        }).then(() => {
            result = true;
        });

        return true;
    }
}
