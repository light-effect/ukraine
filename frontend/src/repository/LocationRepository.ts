import {Axios} from "axios";
import LocationInterface from "@/models/LocationInterface";

export default class LocationRepository {

    private readonly client: Axios;

    constructor(client: Axios) {
        this.client = client;
    }

    public async fetchLocation(slug: string): Promise<LocationInterface> {
        let location: LocationInterface = {
            content: '',
            slug: '',
            name: '',
            id: 0,
            image: null,
        };

        let params: {slug: string, _embed: boolean} = { slug, _embed: true };

        await this.client.get('/location', {params}).then(response => {
            location = {
                id: response.data[0].id,
                content: response.data[0].content.rendered,
                name: response.data[0].title.rendered,
                slug: response.data[0].slug,
                image: response.data[0]._embedded.hasOwnProperty('wp:featuredmedia') ? response.data[0]._embedded['wp:featuredmedia'][0].source_url : null,
            }
        });

        return location;
    }

    public async fetchLocations(): Promise<Array<LocationInterface>> {
        let locations: Array<LocationInterface> = [];

        await this.client.get('/location', { params: {_embed: true} }).then(response =>
            response.data.forEach((location: any) => {
                locations.push({
                    id: location.id,
                    content: location.content.rendered,
                    name: location.title.rendered,
                    slug: location.slug,
                    image: location._embedded.hasOwnProperty('wp:featuredmedia') ? location._embedded['wp:featuredmedia'][0].source_url : null,
                })
            })
        );

        return locations;
    }
}