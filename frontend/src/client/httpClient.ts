import axios from "axios";

export default axios.create({
    baseURL: 'http://localhost:8001/wp-json/wp/v2',
    timeout: 10000,
    headers: {}
});
