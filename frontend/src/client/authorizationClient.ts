import axios from "axios";

export default axios.create({
    baseURL: 'http://localhost:8001/wp-json/jwt-auth/v1',
    timeout: 10000,
    headers: {}
});
