
export default interface LocationInterface {

    id: number;

    name: string;

    content: string;

    slug: string;

    image: string|null;
}