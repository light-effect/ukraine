
export default interface PageInterface {

    id: number;

    title: string;

    content: string;

    slug: string;
}