
export default interface ParamsInterface {

    id?: number,

    page?: number,

    per_page?: number,

    '_embed'?: boolean,

    categories?: Array<number>

    post_type?: string

    slug?: string
}