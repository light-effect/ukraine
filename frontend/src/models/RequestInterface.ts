import LocationInterface from "@/models/LocationInterface";

export default interface RequestInterface {

    fullName: string;

    phone: string;

    location: LocationInterface;

    comment: string;
}
