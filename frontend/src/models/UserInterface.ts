import LocationInterface from "@/models/LocationInterface";
import {UserTypeEnum} from "@/enum/UserTypeEnum";

export default interface UserInterface {

    id?: number;

    firstName?: string;

    lastName?: string;

    username?: string;

    email?: string;

    phone?: string;

    location?: LocationInterface;

    type?: UserTypeEnum;

    information?: string;

    password?: string;

    repeatPassword?: string;
}