import Vue from 'vue'
import Vuex from 'vuex'
import LocationInterface from "@/models/LocationInterface";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    locations: [],
    user: null,
    token: null,
    loading: true,
  },
  mutations: {
    setLocations: (state, locations) => state.locations = locations,
    setUser: (state, user) => state.user = user,
    setToken: (state, token) => state.token = token,
    setLoading: (state, loading) => state.loading = loading,
  },
  actions: {
    setLocations: ({ commit }, locations) => commit('setLocations', locations),
    setUser: ({ commit }, user) => commit('setUser', user),
    setToken: ({ commit }, token) => commit('setToken', token),
    setLoading: ({ commit }, loading) => commit('setLoading', loading),
  },
  getters: {
    locations: (state) => state.locations,
    user: (state) => state.user,
    token: (state) => state.token,
    loading: (state) => state.loading,
    locationBySlug: (state) => (slug: string) => state.locations.find(
        (location: LocationInterface) => location.slug === slug
    ),
  },
  modules: {
  }
})
