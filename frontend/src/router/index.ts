import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Page from '@/views/Page.vue'
import Location from '@/views/Location.vue'
import SignUp from '@/views/SignUp.vue'
import SignIn from "@/views/SignIn.vue";
import Home from "@/views/Home.vue";
import Request from "@/views/Request.vue";
import Volunteers from "@/views/Volunteers.vue";

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    props: {
      slug: 'home'
    }
  },
  {
    path: '/location/:id',
    name: 'Location',
    component: Location,
    props: route => ({slug: route.params.id})
  },
  {
    path: '/sign-up',
    name: 'SignUp',
    component: SignUp
  },
  {
    path: '/sign-in',
    name: 'SignIn',
    component: SignIn
  },
  {
    path: '/request',
    name: 'Request',
    component: Request
  },
  {
    path: '/volunteers',
    name: 'Volunteers',
    component: Volunteers
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
