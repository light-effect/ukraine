<?php

use DevCoder\DotEnv;

(new DotEnv(__DIR__ . '/.env'))->load();

echo getenv('APP_ENV');
// dev
echo getenv('DATABASE_DNS');


define('JWT_AUTH_CORS_ENABLE', true);

define('JWT_AUTH_SECRET_KEY', 'help_ukr_secret');


/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', getenv('DB_NAME') );

/** Database username */
define( 'DB_USER', getenv('DB_USER') );

/** Database password */
define( 'DB_PASSWORD', getenv('DB_PASSWORD') );

/** Database hostname */
define( 'DB_HOST', getenv('DB_HOST') );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '~,&QH^yPyc`n=wp`f[K]-`{)&3~1BR|Rs]f5pPEC7JA2C&<Jb}gib6A&;)hr-704' );
define( 'SECURE_AUTH_KEY',  '-SxM_rz I8iA-)j|_|HxK7OvOGXs^gAnrQ*u:Jvnaf=2cNUVhK1=EQ+@^Jp]1}x[' );
define( 'LOGGED_IN_KEY',    '2->cBHxfEX^cee*Ed%l-x-{kPU3vFYqUKwub~r;gPiqobZ<8^92)HMJ!p.^<Bfx_' );
define( 'NONCE_KEY',        '.Y[St2gqc*h;aLm?bDY3~kW%[36jzv#fc4S?@0_>aEDI}Hx^jD^DWb@Z.8&*n<{ ' );
define( 'AUTH_SALT',        '|ESh ikD+ccM{H+*lC74&2VfV90>>Dv2<G9@.[$4vhH2lJbM3KdnSiAP-+xYWrmV' );
define( 'SECURE_AUTH_SALT', 'H5fbbCFC%@/oK/]C>^oD[:UV,Rk/U;]a`,c}Ep)w&0dRkC }b)MznKBXm0N6pNw ' );
define( 'LOGGED_IN_SALT',   'bg+x2HN<6+]~eB];w{(dL!-WIk+8.>8`BE+=3,ltOahF_[E|6j1b({2R7VhOq1g#' );
define( 'NONCE_SALT',       'r#3h#1ynSKej!5sr)5>/D($WCC@+B{vwUQW,//)?1M~|O6|xu`ZSd?Rw]*~SrZNL' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

define('FS_METHOD', 'direct');

