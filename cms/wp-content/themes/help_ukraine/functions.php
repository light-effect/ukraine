<?php

include 'functions/location.php';
include 'functions/roles.php';
include 'functions/post_type.php';
include 'functions/users_api.php';
include 'functions/volunteers.php';

add_theme_support('post-thumbnails');

register_nav_menus([
    'primary' => esc_html__('Primary menu', 'twentytwentyone'),
    'footer' => esc_html__('Secondary menu', 'twentytwentyone'),
]);

add_filter('jwt_auth_whitelist', function ($endpoints) {
    $your_endpoints = [
        '/wp-json/*',
        '/contact-form-7/v1/*',
    ];

    return array_unique(array_merge($endpoints, $your_endpoints));
});