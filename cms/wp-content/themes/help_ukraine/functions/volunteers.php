<?php

add_action('rest_api_init', function () {
    $namespace = 'wp/v2';

    register_rest_route($namespace, '/volunteers/(?P<id>.*)', [
        'methods' => 'GET',
        'callback' => 'get_volunteer',
    ]);

    register_rest_route($namespace, '/volunteers', [
        'methods' => 'GET',
        'callback' => 'get_volunteers',
    ]);
});

add_action('rest_api_init', function () {

});

function get_volunteer(WP_REST_Request $request)
{
    $posts = get_posts([
        'post_name' => $request['id'],
        'post_type' => 'location',
    ]);

    if (empty($posts)) {
        return new WP_Error('no_location', 'Not found', ['status' => 404]);
    }

    return $posts[0];
}

function get_volunteers(WP_REST_Request $request)
{
    $result = [];
    $users = get_users([
        'role'    => 'volunteer',
        'orderby' => 'user_nicename',
        'order'   => 'ASC'
    ]);

    foreach ($users as $user) {
        $result[] = [
            'id'          => $user->ID,
            'firstName'   => get_user_meta($user->ID, 'first_name')[0],
            'lastName'    => get_user_meta($user->ID, 'last_name')[0],
            'username'    => $user->user_email,
            'email'       => $user->user_email,
            'location'    => get_user_meta($user->ID, 'location')[0],
            'phone'       => get_user_meta($user->ID, 'phone')[0],
            'information' => get_user_meta($user->ID, 'information')[0],
            'type'        => $user->roles[0],
        ];
    }


    if (empty($result)) {
        return new WP_Error('no_location', 'Not found', ['status' => 404]);
    }

    return new WP_REST_Response($result, 200);
}