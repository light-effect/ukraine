<?php


add_action( 'rest_api_init', 'register_api_hooks' );

function register_api_hooks()
{
    $namespace = 'wp/v2';

    register_rest_route($namespace, '/user', [
        'methods' => 'POST',
        'callback' => 'insert_user',
    ]);

    register_rest_route($namespace, '/account',
        [
            'methods'  => 'GET',
            'callback' => 'account',
        ]
    );

    register_rest_route($namespace, '/logout',
        [
            'methods'  => 'GET',
            'callback' => 'logout',
        ]
    );
}

function insert_user(WP_REST_Request $request)
{
    $response = [];

    $parameters = $request->get_json_params();

    $username = $parameters['email'];

    $firstName = sanitize_text_field($parameters['firstName']);

    $lastName = sanitize_text_field($parameters['lastName']);

    $email = sanitize_text_field($parameters['email']);

    $password = sanitize_text_field($parameters['password']);

    $location = sanitize_text_field($parameters['location']);

    $phone = $parameters['phone'];

    $role = sanitize_text_field($parameters['type']);

    $information = sanitize_text_field($parameters['information']);

    $error = new WP_Error();

    if (empty($username)) {
        $error->add(400, __("Username field 'username' is required.", 'wp-rest-user'), array('status' => 400));
        return $error;
    }
    if (empty($email)) {
        $error->add(401, __("Email field 'email' is required.", 'wp-rest-user'), array('status' => 400));
        return $error;
    }
    if (empty($password)) {
        $error->add(404, __("Password field 'password' is required.", 'wp-rest-user'), array('status' => 400));
        return $error;
    }

    $user_id = username_exists($username);
    if ($user_id === false && email_exists($email) == false) {
        $user_id = wp_create_user($username, $password, $email);
        if (is_wp_error($user_id) === false) {
            $user = get_user_by('id', $user_id);

            $user->set_role($role);

            update_user_meta($user_id, 'location', $location);
            update_user_meta($user_id, 'first_name', $firstName);
            update_user_meta($user_id, 'last_name', $lastName);
            update_user_meta($user_id, 'phone', $phone);
            update_user_meta($user_id, 'information', $information);

            $response['code'] = 200;
            $response['message'] = __("User '" . $username . "' Registration was Successful", "wp-rest-user");
        } else {
            return $user_id;
        }
    } else {
        $error->add(406, __("Email already exists, please try 'Reset Password'", 'wp-rest-user'), array('status' => 400));

        return $error;
    }

    wp_signon( ['user_login' => $email, 'user_password' => $password], false );

    return new WP_REST_Response($response, 201);
}

function account($request)
{
    $user = wp_get_current_user();

    $data = [
        'id'        => $user->ID,
        'firstName' => get_user_meta($user->ID, 'first_name')[0],
        'lastName'  => get_user_meta($user->ID, 'last_name')[0],
        'username'  => $user->user_email,
        'email'     => $user->user_email,
        'location'  => get_user_meta($user->ID, 'location')[0],
        'type'      => $user->roles[0],
    ];

    return new WP_REST_Response($data, 200);
}

function logout($request)
{
    wp_logout();

    return new WP_REST_Response([], 200);
}