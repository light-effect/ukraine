<?php

function create_location_type()
{
    register_post_type('location', [
        'labels' => [
            'name' => __('Locations'),
            'singular_name' => __('Location')
        ],
        'public' => true,
        'show_ui'             => true,
        'show_in_nav_menus'   => true,
        'show_in_rest'        => true,
        'hierarchical'        => true,
        'has_archive' => false,
        'supports'            => [ 'title', 'editor', 'thumbnail', 'author' ],
        'rewrite' => ['slug' => 'location'],
    ]);
}

add_action('init', 'create_location_type');